(function(angular) {
    'use strict';

    angular.module('ngBatubox', ['ng'])
            .factory("BatuboxService",
                    function($compile, $rootScope, $http, $templateCache) {

                        var _open = function(options) {
                            $.batubox.showLoading();
                            options = options || {};

                            var window = function() {
                                this.extend = function(data) {
                                    angular.extend(this, data);
                                };
                            };

                            if (angular.isUndefined(options.scope)) {
                                options.scope = $rootScope.$new();
                            }

                            var $window = new window();
                            if (angular.isUndefined(options.id)) {
                                options.id = Math.random().toString(36).substring(7);
                            }

                            options.scope.ID = options.id;

                            $window.ID = options.id;

                            if (!angular.isUndefined(options.template)) {
                                $http.get(options.template, {cache: $templateCache}).then(function(result) {
                                    options.content = $compile(result.data)(options.scope);
                                    $.batubox.open(options);
                                });
                            } else {
                                $.batubox.open(options);
                            }
                            return $window;
                        };

                        var _close = function(ID) {
                            $.batubox.close(ID);
                        };

                        var _confirmbox = function(message, accept, options) {
                            var defaults = {
                                template: "/parts/batubox/confirm.html",
                                scope: $rootScope.$new(),
                                title: {
                                    text: 'Potwierdź'
                                },
                                buttons: {
                                    accept: {
                                        text: 'Ok'
                                    },
                                    cancel: {
                                        text: 'Anuluj'
                                    }
                                }
                            };

                            options = angular.extend({}, defaults, options);
                            options.scope.batubox = {
                                'message': message,
                                'header': options.header,
                                'buttons': options.buttons,
                                'on': {
                                    'accept': accept
                                }
                            };
                            options.scope.batubox.$window = _open(options);
                        };

                        return({
                            open: _open,
                            close: _close,
                            confirmbox: _confirmbox,
                            showLoading: $.batubox.showLoading,
                            hideLoading: $.batubox.hideLoading
                        });
                    }
            )
            .controller('BatuboxConfirm', function($scope, BatuboxService) {
                $scope.batubox = $scope.$parent.batubox;
                var ID = $scope.batubox.$window.ID;

                $scope.cancel = function() {
                    BatuboxService.close(ID);
                };
                $scope.accept = function() {
                    BatuboxService.close(ID);
                    $scope.batubox.on.accept();
                };
            });

})(window.angular);
